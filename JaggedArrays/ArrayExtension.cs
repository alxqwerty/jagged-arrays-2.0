using System;

#pragma warning disable S2368

namespace JaggedArrays
{
    public static class ArrayExtension
    {
        public static void OrderByAscendingBySum(this int[][] source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source), "Array cannot be null.");
            }

            if (source.Length == 1)
            {
                return;
            }

            int?[] sums = new int?[source.Length];

            for (int i = 0; i < source.Length; i++)
            {
                sums[i] = 0;

                if (source[i] is null)
                {
                    sums[i] = null;
                }
                else
                {
                    for (int j = 0; j < source[i].Length; j++)
                    {
                        sums[i] += source[i][j];
                    }
                }
            }

            for (int i = 0; i < source.Length; i++)
            {
                int? container = null;
                int[] arrContainer = null;

                int k = i;

                for (int j = i + 1; j < source.Length; j++)
                {
                    if (sums[i] == null)
                    {
                        SendNullToTheStart(ref source, ref sums, i);
                        break;
                    }

                    if (sums[i] < sums[j])
                    {
                        break;
                    }
                    else if ((sums[i] > sums[j] || sums[j] == null) && i != 0)
                    {
                        if (sums[j] == null)
                        {
                            SendNullToTheStart(ref source, ref sums, j);
                            break;
                        }

                        container = sums[j];
                        arrContainer = source[j];

                        while (container < sums[k - 1] && k > 1)
                        {
                            k--;
                        }

                        if (container < sums[k - 1])
                        {
                            k--;
                        }

                        for (int z = i; z >= k; z--)
                        {
                            source[z + 1] = source[z];
                        }

                        source[k] = arrContainer;

                        for (int z = i; z >= k; z--)
                        {
                            sums[z + 1] = sums[z];
                        }

                        sums[k] = container;
                        break;
                    }
                    else if ((sums[i] > sums[j] || sums[j] == null) && i == 0)
                    {
                        if (sums[j] == null)
                        {
                            SendNullToTheStart(ref source, ref sums, j);
                            break;
                        }

                        container = sums[j];
                        sums[j] = sums[i];
                        sums[i] = container;

                        arrContainer = source[j];
                        source[j] = source[i];
                        source[i] = arrContainer;
                        break;
                    }
                }
            }
        }

        public static void OrderByDescendingBySum(this int[][] source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source), "Array cannot be null.");
            }

            int?[] sums = new int?[source.Length];

            for (int i = 0; i < source.Length; i++)
            {
                sums[i] = 0;

                if (source[i] is null)
                {
                    sums[i] = null;
                }
                else
                {
                    for (int j = 0; j < source[i].Length; j++)
                    {
                        sums[i] += source[i][j];
                    }
                }
            }

            for (int i = 0; i < source.Length; i++)
            {
                int? container = null;
                int[] arrContainer = null;

                int k = i;

                for (int j = i + 1; j < source.Length; j++)
                {
                    if (sums[i] == null)
                    {
                        SendNullToTheEnd(ref source, ref sums, i);
                        break;
                    }

                    if (sums[i] > sums[j])
                    {
                        break;
                    }
                    else if ((sums[i] < sums[j] || sums[j] == null) && i != 0)
                    {
                        if (sums[j] == null)
                        {
                            SendNullToTheEnd(ref source, ref sums, j);
                            break;
                        }

                        container = sums[j];
                        arrContainer = source[j];

                        while (container > sums[k - 1] && k > 1)
                        {
                            k--;
                        }

                        if (container > sums[k - 1])
                        {
                            k--;
                        }

                        for (int z = i; z >= k; z--)
                        {
                            source[z + 1] = source[z];
                        }

                        source[k] = arrContainer;

                        for (int z = i; z >= k; z--)
                        {
                            sums[z + 1] = sums[z];
                        }

                        sums[k] = container;
                        break;
                    }
                    else if ((sums[i] < sums[j] || sums[j] == null) && i == 0)
                    {
                        if (sums[j] == null)
                        {
                            SendNullToTheEnd(ref source, ref sums, j);
                            break;
                        }

                        container = sums[j];
                        sums[j] = sums[i];
                        sums[i] = container;

                        arrContainer = source[j];
                        source[j] = source[i];
                        source[i] = arrContainer;
                        break;
                    }
                }
            }
        }

        public static void OrderByAscendingByMax(this int[][] source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source), "Array cannot be null.");
            }

            if (source.Length == 1)
            {
                return;
            }

            int?[] maxValues = new int?[source.Length];

            for (int i = 0; i < source.Length; i++)
            {
                maxValues[i] = 0;

                if (source[i] is null)
                {
                    maxValues[i] = null;
                }
                else
                {
                    for (int j = 0; j < source[i].Length; j++)
                    {
                        if (maxValues[i] < source[i][j])
                        {
                            maxValues[i] = source[i][j];
                        }
                    }
                }
            }

            for (int i = 0; i < source.Length; i++)
            {
                int? container = null;
                int[] arrContainer = null;

                int k = i;

                for (int j = i + 1; j < source.Length; j++)
                {
                    if (maxValues[i] == null)
                    {
                        SendNullToTheStart(ref source, ref maxValues, i);
                        break;
                    }

                    if (maxValues[i] < maxValues[j])
                    {
                        break;
                    }
                    else if ((maxValues[i] > maxValues[j] || maxValues[j] == null) && i != 0)
                    {
                        if (maxValues[j] == null)
                        {
                            SendNullToTheStart(ref source, ref maxValues, j);
                            break;
                        }

                        container = maxValues[j];
                        arrContainer = source[j];

                        while (container < maxValues[k - 1] && k > 1)
                        {
                            k--;
                        }

                        if (container < maxValues[k - 1])
                        {
                            k--;
                        }

                        for (int z = i; z >= k; z--)
                        {
                            source[z + 1] = source[z];
                        }

                        source[k] = arrContainer;

                        for (int z = i; z >= k; z--)
                        {
                            maxValues[z + 1] = maxValues[z];
                        }

                        maxValues[k] = container;
                        break;
                    }
                    else if ((maxValues[i] > maxValues[j] || maxValues[j] == null) && i == 0)
                    {
                        if (maxValues[j] == null)
                        {
                            SendNullToTheStart(ref source, ref maxValues, j);
                            break;
                        }

                        container = maxValues[j];
                        maxValues[j] = maxValues[i];
                        maxValues[i] = container;

                        arrContainer = source[j];
                        source[j] = source[i];
                        source[i] = arrContainer;
                        break;
                    }
                }
            }
        }

        public static void OrderByDescendingByMax(this int[][] source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source), "Array cannot be null.");
            }

            if (source.Length == 1)
            {
                return;
            }

            int?[] maxValues = new int?[source.Length];

            for (int i = 0; i < source.Length; i++)
            {
                maxValues[i] = 0;

                if (source[i] is null)
                {
                    maxValues[i] = null;
                }
                else
                {
                    for (int j = 0; j < source[i].Length; j++)
                    {
                        if (maxValues[i] < source[i][j])
                        {
                            maxValues[i] = source[i][j];
                        }
                    }
                }
            }

            for (int i = 0; i < source.Length; i++)
            {
                int? container = null;
                int[] arrContainer = null;

                int k = i;

                for (int j = i + 1; j < source.Length; j++)
                {
                    if (maxValues[i] == null)
                    {
                        SendNullToTheEnd(ref source, ref maxValues, i);
                        break;
                    }

                    if (maxValues[i] > maxValues[j])
                    {
                        break;
                    }
                    else if ((maxValues[i] < maxValues[j] || maxValues[j] == null) && i != 0)
                    {
                        if (maxValues[j] == null)
                        {
                            SendNullToTheEnd(ref source, ref maxValues, j);
                            break;
                        }

                        container = maxValues[j];
                        arrContainer = source[j];

                        while (container > maxValues[k - 1] && k > 1)
                        {
                            k--;
                        }

                        if (container > maxValues[k - 1])
                        {
                            k--;
                        }

                        for (int z = i; z >= k; z--)
                        {
                            source[z + 1] = source[z];
                        }

                        source[k] = arrContainer;

                        for (int z = i; z >= k; z--)
                        {
                            maxValues[z + 1] = maxValues[z];
                        }

                        maxValues[k] = container;
                        break;
                    }
                    else if ((maxValues[i] < maxValues[j] || maxValues[j] == null) && i == 0)
                    {
                        if (maxValues[j] == null)
                        {
                            SendNullToTheEnd(ref source, ref maxValues, j);
                            break;
                        }

                        container = maxValues[j];
                        maxValues[j] = maxValues[i];
                        maxValues[i] = container;

                        arrContainer = source[j];
                        source[j] = source[i];
                        source[i] = arrContainer;
                        break;
                    }
                }
            }
        }

        public static void SendNullToTheStart(ref int[][] array, ref int?[] calculations, int currentIndex)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null.");
            }

            if (calculations is null)
            {
                throw new ArgumentNullException(nameof(calculations), "Array cannot be null.");
            }

            int? container = null;
            int[] arrContainer = null;

            if (calculations[currentIndex] == null)
            {
                arrContainer = array[currentIndex];

                for (int i = currentIndex; i > 0; i--)
                {
                    array[i] = array[i - 1];
                }

                array[0] = arrContainer;

                if (currentIndex == calculations.Length - 1)
                {
                    return;
                }

                container = calculations[currentIndex];

                for (int i = currentIndex; i > 0; i--)
                {
                    calculations[i] = calculations[i - 1];
                }

                calculations[0] = container;
            }
        }

        public static void SendNullToTheEnd(ref int[][] array, ref int?[] calculations, int currentIndex)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null.");
            }

            if (calculations is null)
            {
                throw new ArgumentNullException(nameof(calculations), "Array cannot be null.");
            }

            int? container = null;
            int[] arrContainer = null;

            if (calculations[currentIndex] == null)
            {
                arrContainer = array[currentIndex];

                for (int i = currentIndex; i < array.Length - 1; i++)
                {
                    array[i] = array[i + 1];
                }

                array[^1] = arrContainer;

                if (currentIndex == calculations.Length - 1)
                {
                    return;
                }

                container = calculations[currentIndex];

                for (int i = currentIndex; i < array.Length - 1; i++)
                {
                    calculations[i] = calculations[i + 1];
                }

                calculations[^1] = container;
            }
        }
    }
}
